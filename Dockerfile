FROM centos:latest
#MAINTAINER kaci.juba@yahoo.FROM
RUN yum install -y https \
zip\
unzip

ADD https://www.free-css.com/assets/files/free-css-templates/download/page254/photogenic.zip /var/www/html/
WORKDIR /var/www/html/
RUN unzip photogenic.zip
RUN cp -rvf photogenic/* .

RUN rm -rf photogenic photogenic.zip
CMD [ "/usr/sbon/httpd", "-D","FORGROUND" ]
EXPOSE 80